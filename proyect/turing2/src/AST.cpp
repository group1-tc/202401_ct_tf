#include "AST.h"
#include "atn/BlockStartState.h"
#include <iostream>
#include <llvm-18/llvm/IR/Constants.h>
#include <llvm-18/llvm/IR/Value.h>
#include <llvm-18/llvm/IR/IRBuilder.h>
#include <llvm-18/llvm/IR/Module.h>
#include <map>
#include <string>

void ASTchar::print() const {
    std::cout << c;
}

void ASTaction::print() const {
    if(action == 'L' || action == 'R') std::cout << predicate << "[label=\"" << action << " <- ";
}

void ASTinstruction::print() const {
    for(const auto& a : actions) {
        a->print();
    }
    for(auto c : condition) {
        std::cout << c << " ";
    }
    std::cout << "\"]\n";
}

void ASTname::print() const {
    std::cout << name;
}

void ASTstate::print() const {
    for (const auto& instruction : instructions) {
        std::cout << "\t" << name << " -> ";
        instruction->print();
    }
}

void ASTtable::print() const {
    for (const auto& state : states) {
        state->print();
    }
}

void ASTblank::print() const {
    std::cout << "blank =" << c << "\n";
}

void ASTtape::print() const {
    std::cout << "tape = " << tape << "\n";
}

void ASTprogram::print() const {
    std::cout << "digraph {\n";
    for (const auto& state : table) {
        state->print();
    }
    std::cout << "}\n";
}

void ASTarray::print() const {
    std::cout << "[ ";
    for (const auto& element : array) {
        std::cout << element << " ";
    }
    std::cout << " ]\n";
}

void ASTstart::print() const {
    std::cout << "start = " << start << "\n";
}

void ASTpredicate::print() const {
    for (const auto& action : actions) {
        action->print();
    }
}

void ASTstate::fill() {
    for (const auto& instruction : instructions) {
        instruction->fill(name);
    }
}

void ASTinstruction::fill(std::string name) {
    for (const auto& action : actions) {
        action->setPredicate(name);
    }
}

llvm::Value* ASTblank::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTtape::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTstart::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTaction::codegen(
    llvm::LLVMContext &context, 
    llvm::IRBuilder<> &builder, 
    llvm::Module &module, 
    std::map<std::string, llvm::BasicBlock*> &blocks, 
    llvm::Value* arrayPtr, 
    llvm::Value* &pointer
) {
    //std::cout << "ASTaction::codegen called for action: " << action << std::endl;

    if (action == 'W') {
        //std::cout << "Action: Write" << std::endl;
        // Write action
        llvm::Value* charValue = llvm::ConstantInt::get(llvm::Type::getInt8Ty(context), predicate[0]);
        builder.CreateStore(charValue, pointer);
        //std::cout << "Character written" << std::endl;
    }

    return nullptr;
}

llvm::Value* ASTinstruction::codegen(
    llvm::LLVMContext &context, 
    llvm::IRBuilder<> &builder, 
    llvm::Module &module, 
    std::map<std::string, llvm::BasicBlock*> &blocks, 
    llvm::Value* arrayPtr, 
    llvm::Value* &pointer
) {
    llvm::Function *currentFunction = builder.GetInsertBlock()->getParent();

    for (const auto& cond : condition) {
        llvm::BasicBlock *thenBlock = llvm::BasicBlock::Create(context, "then", currentFunction);
        llvm::BasicBlock *mergeBlock = llvm::BasicBlock::Create(context, "ifcont", currentFunction);

        llvm::Value* loadedChar = builder.CreateLoad(llvm::Type::getInt8Ty(context), pointer, "loadedChar");

        llvm::Value* condition = builder.CreateICmpEQ(loadedChar, llvm::ConstantInt::get(llvm::Type::getInt8Ty(context), cond), "cmp");

        builder.CreateCondBr(condition, thenBlock, mergeBlock);

        builder.SetInsertPoint(thenBlock);
        for (const auto& action : actions) {
            action->codegen(context, builder, module, blocks, arrayPtr, pointer);
        }
        builder.CreateBr(mergeBlock);

        builder.SetInsertPoint(mergeBlock);
    }

    return nullptr;
}

llvm::Value* ASTstate::codegen(
    llvm::LLVMContext &context, 
    llvm::IRBuilder<> &builder, 
    llvm::Module &module, 
    std::map<std::string, llvm::BasicBlock*> &blocks, 
    llvm::Value* arrayPtr, 
    llvm::Value* &pointer
) {
    llvm::BasicBlock *currentBlock = blocks[getName()];
    builder.SetInsertPoint(currentBlock);

    for (const auto& instruction : instructions) {
        instruction->codegen(context, builder, module, blocks, arrayPtr, pointer);
    }

    builder.CreateBr(blocks["exit"]);

    return nullptr;
}

llvm::Value* ASTtable::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTprogram::codegen(
    llvm::LLVMContext &context, 
    llvm::IRBuilder<> &builder, 
    llvm::Module &module, 
    std::map<std::string, llvm::BasicBlock*> &blocks, 
    llvm::Value* arrayPtr, 
    llvm::Value* &pointer
) {
    llvm::FunctionType *funcType = llvm::FunctionType::get(builder.getVoidTy(), false);
    llvm::Function *mainFunction = llvm::Function::Create(funcType, llvm::Function::ExternalLinkage, "main", &module);
    
    llvm::BasicBlock *entry = llvm::BasicBlock::Create(context, "entry", mainFunction);
    builder.SetInsertPoint(entry);

    // Tape
    const int tapeLength = 100; 
    const int middleIndex = tapeLength / 2;

    // Array 
    llvm::ArrayType* arrayType = llvm::ArrayType::get(llvm::Type::getInt8Ty(context), tapeLength);

    // Allocate the array
    arrayPtr = builder.CreateAlloca(arrayType, nullptr, "tapeArray");

    // Initialize the array
    llvm::Value* blankChar = llvm::ConstantInt::get(llvm::Type::getInt8Ty(context), blank);
    for (int i = 0; i < tapeLength; ++i) {
        llvm::Value* index = llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), i);
        llvm::Value* elementPtr = builder.CreateGEP(arrayType, arrayPtr, {builder.getInt32(0), index});
        builder.CreateStore(blankChar, elementPtr);
    }

    // Insert the string 
    for (size_t i = 0; i < tape.size(); ++i) {
        llvm::Value* index = llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), middleIndex + i);
        llvm::Value* elementPtr = builder.CreateGEP(arrayType, arrayPtr, {builder.getInt32(0), index});
        llvm::Value* charValue = llvm::ConstantInt::get(llvm::Type::getInt8Ty(context), tape[i]);
        builder.CreateStore(charValue, elementPtr);
    }

    // Return the pointer to the middle
    llvm::Value* middleIndexValue = llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), middleIndex);
    pointer = builder.CreateGEP(arrayType, arrayPtr, {builder.getInt32(0), middleIndexValue});

    // Create blocks for states
    for (const auto& state : table) {
        std::string name = state->getName();
        blocks[name] = llvm::BasicBlock::Create(context, name, mainFunction);
    }

    // Add return in exit block
    blocks["exit"] = llvm::BasicBlock::Create(context, "exit", mainFunction);
    builder.SetInsertPoint(blocks["exit"]);
    builder.CreateRetVoid();

    // Jump to the start
    builder.SetInsertPoint(entry);
    builder.CreateBr(blocks[start]);

    // code for each state
    for (const auto& state : table) {
        state->codegen(context, builder, module, blocks, arrayPtr, pointer);
    }

    return pointer;
}

llvm::Value* ASTarray::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTpredicate::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTname::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}

llvm::Value* ASTchar::codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer){
    return nullptr;
}
