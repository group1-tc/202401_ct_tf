#include "CodeGen.h"
#include <llvm-18/llvm/IR/BasicBlock.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Support/raw_ostream.h>
#include <fstream>
#include <map>

void generateLLVMIR(std::unique_ptr<ASTprogram> &ast, const std::string &filename) {
    llvm::LLVMContext context;
    llvm::IRBuilder<> builder(context);
    llvm::Module module("TuringAutomata", context);

    std::map<std::string, llvm::BasicBlock*> blocks;

    llvm::Value* pointer = nullptr;

    llvm::Value* programValue = ast->codegen(context, builder, module, blocks , nullptr, pointer);

    if (llvm::verifyModule(module, &llvm::errs())) {
        throw std::runtime_error("Generated LLVM IR is invalid!");
    }

    std::error_code EC;
    llvm::raw_fd_ostream dest(filename, EC);
    

    if (EC) {
        throw std::runtime_error("Could not open file: " + EC.message());
    }

    module.print(dest, nullptr);
    dest.flush();
}
