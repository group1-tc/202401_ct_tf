#include "Parser.h"
#include "antlr4-runtime.h"
#include "turingLexer.h"
#include "turingParser.h"
#include "AST.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <memory>
#include <vector>
#include <iostream>

using namespace antlr4;

std::string readFile(const std::string& filename) {
    std::ifstream sourceFile(filename);
    if (!sourceFile.is_open()) {
        throw std::runtime_error("Could not open file " + filename);
    }
    // std::cout << "file opened\n";
    return std::string(std::istreambuf_iterator<char>(sourceFile), std::istreambuf_iterator<char>());
}

std::unique_ptr<ASTchar> visitNode(turingParser::LiteralContext *ctx) {
    // std::cout << "literal\n";
    if(ctx->NUM()) {
        return std::make_unique<ASTchar>(ctx->NUM()->getText()[0]);
    } else if(ctx->CHAR()) {
        return std::make_unique<ASTchar>(ctx->CHAR()->getText()[1]);
    } else {
        throw std::runtime_error("Unknown literal type");
    }
}

std::unique_ptr<ASTarray> visitNode(turingParser::ArrayContext *ctx) {
    // std::cout << "array\n";
    std::vector<char> elements;
    for(auto element : ctx->literal()) {
        elements.push_back(visitNode(element)->getChar());
    }
    return std::make_unique<ASTarray>(std::move(elements));
}

std::unique_ptr<ASTarray> visitNode(turingParser::SubContext *ctx) {
    // std::cout << "sub\n";
    if(ctx->array()) {
        return visitNode(ctx->array());
    } else if (ctx->literal()) {
        return std::make_unique<ASTarray>(std::vector<char>(1, visitNode(ctx->literal())->getChar()));
    } else {
        throw std::runtime_error("Unknown sub type");
    }
}

std::unique_ptr<ASTaction> visitNode(turingParser::UnaryContext* ctx) {
    // std::cout << "unary\n";
    if(ctx->MOVE())
        return std::make_unique<ASTaction>(ctx->MOVE()->getText()[0], "");
    else
        throw std::runtime_error("Unknown unary type");
}

std::unique_ptr<ASTaction> visitNode(turingParser::BinaryContext* ctx) {
    // std::cout << "binary\n";
    if(ctx->WRITE())
        return std::make_unique<ASTaction>(ctx->WRITE()->getText()[0], std::string(1, visitNode(ctx->literal())->getChar()));
    else if(ctx->MOVE())
        return std::make_unique<ASTaction>(ctx->MOVE()->getText()[0], ctx->STATE()->getText());
    else
        throw std::runtime_error("Unknown binary type");
}

std::unique_ptr<ASTpredicate> visitNode(turingParser::PredContext* ctx) {
    // std::cout << "pred\n";
    std::vector<std::unique_ptr<ASTaction>> actions;
    for(auto action : ctx->unary()) {
        actions.push_back(std::move(visitNode(action)));
    }
    for (auto action : ctx->binary()) {
        actions.push_back(std::move(visitNode(action)));
    }
    return std::make_unique<ASTpredicate>(std::move(actions));
}

std::unique_ptr<ASTinstruction> visitNode(turingParser::ActionContext *ctx) {
    // std::cout << "action\n";
    return std::make_unique<ASTinstruction>(visitNode(ctx->sub())->getArray(), std::move(visitNode(ctx->pred())->getActions()));
}

std::unique_ptr<ASTstate> visitNode(turingParser::StatContext *ctx) {
    // std::cout << "state\n";
    std::string name = ctx->STATE()->getText();
    std::vector<std::unique_ptr<ASTinstruction>> instructions;
    for(auto instruction : ctx->action()) {
        instructions.push_back(std::move(visitNode(instruction)));
    }
    auto state = std::make_unique<ASTstate>(std::move(name), std::move(instructions));
    state->fill();
    return state;
}

std::unique_ptr<ASTtable> visitNode(turingParser::TableContext *ctx) {
    // std::cout << "table\n";
    std::vector<std::unique_ptr<ASTstate>> states;
    for(auto state : ctx->stat()) {
        states.push_back(std::move(visitNode(state)));
    }
    if (states.empty()) {
        throw std::runtime_error("No states in table");
    }
    return std::make_unique<ASTtable>(std::move(states));
}

std::unique_ptr<ASTblank> visitNode(turingParser::BlankContext *ctx) {
    // std::cout << "blank\n";
    return std::make_unique<ASTblank>(visitNode(ctx->literal())->getChar());
}

std::unique_ptr<ASTstart> visitNode(turingParser::StartContext *ctx) {
    // std::cout << "start\n";
    return std::make_unique<ASTstart>(ctx->STATE()->getText());
}

std::unique_ptr<ASTtape> visitNode(turingParser::InputContext *ctx) {
    // std::cout << "input\n";
    return std::make_unique<ASTtape>(ctx->STR()->getText().substr(1, ctx->STR()->getText().size() - 2));
}

std::unique_ptr<ASTprogram> visitNode(turingParser::ProgramContext *ctx) {
    // std::cout << "program\n";
    char blank;
    std::string start, tape = "";
    if(ctx->blank()) {
        blank = visitNode(ctx->blank())->getChar();
    }else {
        throw std::runtime_error("No blank character");
    }
    if(ctx->input()) {
        tape = visitNode(ctx->input())->getTape();
    }
    if(ctx->start()) {
        start = visitNode(ctx->start())->getStart();
    } else {
        throw std::runtime_error("No start state");
    }
    return std::make_unique<ASTprogram>(std::move(visitNode(ctx->table())->getStates()), blank, std::move(start), std::move(tape));
}


std::unique_ptr<ASTprogram> generateAST(const std::string& filename) {
    std::string source = readFile(filename);
    ANTLRInputStream input(source);
    turingLexer lexer(&input);
    CommonTokenStream tokens(&lexer);
    turingParser parser(&tokens);
    return visitNode(parser.program());
}
