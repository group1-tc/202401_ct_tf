#include <iostream>
#include "Parser.h"
#include "CodeGen.h"

int main(int argc, char *argv[]) {
    std::cout << "Turing machin graph compiler\n";
    std::cout << "Compiler Theory - UPC 2024-1\n";
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <source-file>\n";
        return 1;
    }
    try {
        std::unique_ptr<ASTprogram> ast = generateAST(argv[1]);
        if (!ast) {
            std::cerr << "Error generating AST.\n";
            return 1;
        }

        ast->print();
        std::cout << std::endl;

        std::string filename = "out.ll";

        generateLLVMIR(ast, filename);
    } catch (const std::exception &e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}
