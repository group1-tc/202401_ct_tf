
// Generated from turing.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "turingListener.h"


/**
 * This class provides an empty implementation of turingListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  turingBaseListener : public turingListener {
public:

  virtual void enterProgram(turingParser::ProgramContext * /*ctx*/) override { }
  virtual void exitProgram(turingParser::ProgramContext * /*ctx*/) override { }

  virtual void enterTable(turingParser::TableContext * /*ctx*/) override { }
  virtual void exitTable(turingParser::TableContext * /*ctx*/) override { }

  virtual void enterStart(turingParser::StartContext * /*ctx*/) override { }
  virtual void exitStart(turingParser::StartContext * /*ctx*/) override { }

  virtual void enterBlank(turingParser::BlankContext * /*ctx*/) override { }
  virtual void exitBlank(turingParser::BlankContext * /*ctx*/) override { }

  virtual void enterInput(turingParser::InputContext * /*ctx*/) override { }
  virtual void exitInput(turingParser::InputContext * /*ctx*/) override { }

  virtual void enterStat(turingParser::StatContext * /*ctx*/) override { }
  virtual void exitStat(turingParser::StatContext * /*ctx*/) override { }

  virtual void enterAction(turingParser::ActionContext * /*ctx*/) override { }
  virtual void exitAction(turingParser::ActionContext * /*ctx*/) override { }

  virtual void enterPred(turingParser::PredContext * /*ctx*/) override { }
  virtual void exitPred(turingParser::PredContext * /*ctx*/) override { }

  virtual void enterUnary(turingParser::UnaryContext * /*ctx*/) override { }
  virtual void exitUnary(turingParser::UnaryContext * /*ctx*/) override { }

  virtual void enterBinary(turingParser::BinaryContext * /*ctx*/) override { }
  virtual void exitBinary(turingParser::BinaryContext * /*ctx*/) override { }

  virtual void enterSub(turingParser::SubContext * /*ctx*/) override { }
  virtual void exitSub(turingParser::SubContext * /*ctx*/) override { }

  virtual void enterLiteral(turingParser::LiteralContext * /*ctx*/) override { }
  virtual void exitLiteral(turingParser::LiteralContext * /*ctx*/) override { }

  virtual void enterArray(turingParser::ArrayContext * /*ctx*/) override { }
  virtual void exitArray(turingParser::ArrayContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

