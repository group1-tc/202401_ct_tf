
// Generated from turing.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"




class  turingLexer : public antlr4::Lexer {
public:
  enum {
    WS = 1, COMMENT = 2, TABLE = 3, WRITE = 4, BLANK = 5, INP = 6, ST = 7, 
    ENDL = 8, MOVE = 9, STATE = 10, CHAR = 11, NUM = 12, STR = 13, LB = 14, 
    RB = 15, LC = 16, RC = 17, COMMA = 18, COLON = 19
  };

  explicit turingLexer(antlr4::CharStream *input);

  ~turingLexer() override;


  std::string getGrammarFileName() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const std::vector<std::string>& getChannelNames() const override;

  const std::vector<std::string>& getModeNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;

  const antlr4::atn::ATN& getATN() const override;

  // By default the static state used to implement the lexer is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:

  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

};

