
// Generated from turing.g4 by ANTLR 4.13.1


#include "turingListener.h"

#include "turingParser.h"


using namespace antlrcpp;

using namespace antlr4;

namespace {

struct TuringParserStaticData final {
  TuringParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  TuringParserStaticData(const TuringParserStaticData&) = delete;
  TuringParserStaticData(TuringParserStaticData&&) = delete;
  TuringParserStaticData& operator=(const TuringParserStaticData&) = delete;
  TuringParserStaticData& operator=(TuringParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag turingParserOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
TuringParserStaticData *turingParserStaticData = nullptr;

void turingParserInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (turingParserStaticData != nullptr) {
    return;
  }
#else
  assert(turingParserStaticData == nullptr);
#endif
  auto staticData = std::make_unique<TuringParserStaticData>(
    std::vector<std::string>{
      "program", "table", "start", "blank", "input", "stat", "action", "pred", 
      "unary", "binary", "sub", "literal", "array"
    },
    std::vector<std::string>{
      "", "", "", "'table'", "'write'", "'blank'", "'input'", "'start state'", 
      "'\\n'", "", "", "", "", "", "'['", "']'", "'{'", "'}'", "','", "':'"
    },
    std::vector<std::string>{
      "", "WS", "COMMENT", "TABLE", "WRITE", "BLANK", "INP", "ST", "ENDL", 
      "MOVE", "STATE", "CHAR", "NUM", "STR", "LB", "RB", "LC", "RC", "COMMA", 
      "COLON"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,19,156,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,6,2,
  	7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,1,0,3,0,28,8,0,1,
  	0,3,0,31,8,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,4,1,41,8,1,11,1,12,1,42,
  	1,1,5,1,46,8,1,10,1,12,1,49,9,1,1,2,1,2,1,2,1,2,4,2,55,8,2,11,2,12,2,
  	56,1,3,1,3,1,3,1,3,4,3,63,8,3,11,3,12,3,64,1,4,1,4,1,4,1,4,4,4,71,8,4,
  	11,4,12,4,72,1,5,1,5,1,5,1,5,4,5,79,8,5,11,5,12,5,80,1,5,1,5,1,5,1,5,
  	4,5,87,8,5,11,5,12,5,88,1,5,4,5,92,8,5,11,5,12,5,93,1,5,1,5,1,5,4,5,99,
  	8,5,11,5,12,5,100,3,5,103,8,5,1,6,1,6,1,6,1,6,1,7,1,7,1,7,3,7,112,8,7,
  	1,7,1,7,1,7,3,7,117,8,7,5,7,119,8,7,10,7,12,7,122,9,7,1,7,1,7,1,7,3,7,
  	127,8,7,1,8,1,8,1,9,1,9,1,9,1,9,1,9,1,9,3,9,137,8,9,1,10,1,10,3,10,141,
  	8,10,1,11,1,11,1,12,1,12,1,12,1,12,5,12,149,8,12,10,12,12,12,152,9,12,
  	1,12,1,12,1,12,0,0,13,0,2,4,6,8,10,12,14,16,18,20,22,24,0,1,1,0,11,12,
  	162,0,27,1,0,0,0,2,36,1,0,0,0,4,50,1,0,0,0,6,58,1,0,0,0,8,66,1,0,0,0,
  	10,102,1,0,0,0,12,104,1,0,0,0,14,126,1,0,0,0,16,128,1,0,0,0,18,136,1,
  	0,0,0,20,140,1,0,0,0,22,142,1,0,0,0,24,144,1,0,0,0,26,28,3,8,4,0,27,26,
  	1,0,0,0,27,28,1,0,0,0,28,30,1,0,0,0,29,31,3,6,3,0,30,29,1,0,0,0,30,31,
  	1,0,0,0,31,32,1,0,0,0,32,33,3,4,2,0,33,34,3,2,1,0,34,35,5,0,0,1,35,1,
  	1,0,0,0,36,37,5,3,0,0,37,38,5,19,0,0,38,40,5,8,0,0,39,41,3,10,5,0,40,
  	39,1,0,0,0,41,42,1,0,0,0,42,40,1,0,0,0,42,43,1,0,0,0,43,47,1,0,0,0,44,
  	46,5,8,0,0,45,44,1,0,0,0,46,49,1,0,0,0,47,45,1,0,0,0,47,48,1,0,0,0,48,
  	3,1,0,0,0,49,47,1,0,0,0,50,51,5,7,0,0,51,52,5,19,0,0,52,54,5,10,0,0,53,
  	55,5,8,0,0,54,53,1,0,0,0,55,56,1,0,0,0,56,54,1,0,0,0,56,57,1,0,0,0,57,
  	5,1,0,0,0,58,59,5,5,0,0,59,60,5,19,0,0,60,62,3,22,11,0,61,63,5,8,0,0,
  	62,61,1,0,0,0,63,64,1,0,0,0,64,62,1,0,0,0,64,65,1,0,0,0,65,7,1,0,0,0,
  	66,67,5,6,0,0,67,68,5,19,0,0,68,70,5,13,0,0,69,71,5,8,0,0,70,69,1,0,0,
  	0,71,72,1,0,0,0,72,70,1,0,0,0,72,73,1,0,0,0,73,9,1,0,0,0,74,75,5,10,0,
  	0,75,76,5,19,0,0,76,78,3,12,6,0,77,79,5,8,0,0,78,77,1,0,0,0,79,80,1,0,
  	0,0,80,78,1,0,0,0,80,81,1,0,0,0,81,103,1,0,0,0,82,83,5,10,0,0,83,86,5,
  	19,0,0,84,85,5,8,0,0,85,87,3,12,6,0,86,84,1,0,0,0,87,88,1,0,0,0,88,86,
  	1,0,0,0,88,89,1,0,0,0,89,91,1,0,0,0,90,92,5,8,0,0,91,90,1,0,0,0,92,93,
  	1,0,0,0,93,91,1,0,0,0,93,94,1,0,0,0,94,103,1,0,0,0,95,96,5,10,0,0,96,
  	98,5,19,0,0,97,99,5,8,0,0,98,97,1,0,0,0,99,100,1,0,0,0,100,98,1,0,0,0,
  	100,101,1,0,0,0,101,103,1,0,0,0,102,74,1,0,0,0,102,82,1,0,0,0,102,95,
  	1,0,0,0,103,11,1,0,0,0,104,105,3,20,10,0,105,106,5,19,0,0,106,107,3,14,
  	7,0,107,13,1,0,0,0,108,111,5,16,0,0,109,112,3,16,8,0,110,112,3,18,9,0,
  	111,109,1,0,0,0,111,110,1,0,0,0,112,120,1,0,0,0,113,116,5,18,0,0,114,
  	117,3,16,8,0,115,117,3,18,9,0,116,114,1,0,0,0,116,115,1,0,0,0,117,119,
  	1,0,0,0,118,113,1,0,0,0,119,122,1,0,0,0,120,118,1,0,0,0,120,121,1,0,0,
  	0,121,123,1,0,0,0,122,120,1,0,0,0,123,124,5,17,0,0,124,127,1,0,0,0,125,
  	127,3,16,8,0,126,108,1,0,0,0,126,125,1,0,0,0,127,15,1,0,0,0,128,129,5,
  	9,0,0,129,17,1,0,0,0,130,131,5,9,0,0,131,132,5,19,0,0,132,137,5,10,0,
  	0,133,134,5,4,0,0,134,135,5,19,0,0,135,137,3,22,11,0,136,130,1,0,0,0,
  	136,133,1,0,0,0,137,19,1,0,0,0,138,141,3,22,11,0,139,141,3,24,12,0,140,
  	138,1,0,0,0,140,139,1,0,0,0,141,21,1,0,0,0,142,143,7,0,0,0,143,23,1,0,
  	0,0,144,145,5,14,0,0,145,150,3,22,11,0,146,147,5,18,0,0,147,149,3,22,
  	11,0,148,146,1,0,0,0,149,152,1,0,0,0,150,148,1,0,0,0,150,151,1,0,0,0,
  	151,153,1,0,0,0,152,150,1,0,0,0,153,154,5,15,0,0,154,25,1,0,0,0,19,27,
  	30,42,47,56,64,72,80,88,93,100,102,111,116,120,126,136,140,150
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  turingParserStaticData = staticData.release();
}

}

turingParser::turingParser(TokenStream *input) : turingParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

turingParser::turingParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  turingParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *turingParserStaticData->atn, turingParserStaticData->decisionToDFA, turingParserStaticData->sharedContextCache, options);
}

turingParser::~turingParser() {
  delete _interpreter;
}

const atn::ATN& turingParser::getATN() const {
  return *turingParserStaticData->atn;
}

std::string turingParser::getGrammarFileName() const {
  return "turing.g4";
}

const std::vector<std::string>& turingParser::getRuleNames() const {
  return turingParserStaticData->ruleNames;
}

const dfa::Vocabulary& turingParser::getVocabulary() const {
  return turingParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView turingParser::getSerializedATN() const {
  return turingParserStaticData->serializedATN;
}


//----------------- ProgramContext ------------------------------------------------------------------

turingParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

turingParser::StartContext* turingParser::ProgramContext::start() {
  return getRuleContext<turingParser::StartContext>(0);
}

turingParser::TableContext* turingParser::ProgramContext::table() {
  return getRuleContext<turingParser::TableContext>(0);
}

tree::TerminalNode* turingParser::ProgramContext::EOF() {
  return getToken(turingParser::EOF, 0);
}

turingParser::InputContext* turingParser::ProgramContext::input() {
  return getRuleContext<turingParser::InputContext>(0);
}

turingParser::BlankContext* turingParser::ProgramContext::blank() {
  return getRuleContext<turingParser::BlankContext>(0);
}


size_t turingParser::ProgramContext::getRuleIndex() const {
  return turingParser::RuleProgram;
}

void turingParser::ProgramContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProgram(this);
}

void turingParser::ProgramContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProgram(this);
}

turingParser::ProgramContext* turingParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, turingParser::RuleProgram);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(27);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == turingParser::INP) {
      setState(26);
      input();
    }
    setState(30);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == turingParser::BLANK) {
      setState(29);
      blank();
    }
    setState(32);
    start();
    setState(33);
    table();
    setState(34);
    match(turingParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TableContext ------------------------------------------------------------------

turingParser::TableContext::TableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::TableContext::TABLE() {
  return getToken(turingParser::TABLE, 0);
}

tree::TerminalNode* turingParser::TableContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

std::vector<tree::TerminalNode *> turingParser::TableContext::ENDL() {
  return getTokens(turingParser::ENDL);
}

tree::TerminalNode* turingParser::TableContext::ENDL(size_t i) {
  return getToken(turingParser::ENDL, i);
}

std::vector<turingParser::StatContext *> turingParser::TableContext::stat() {
  return getRuleContexts<turingParser::StatContext>();
}

turingParser::StatContext* turingParser::TableContext::stat(size_t i) {
  return getRuleContext<turingParser::StatContext>(i);
}


size_t turingParser::TableContext::getRuleIndex() const {
  return turingParser::RuleTable;
}

void turingParser::TableContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTable(this);
}

void turingParser::TableContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTable(this);
}

turingParser::TableContext* turingParser::table() {
  TableContext *_localctx = _tracker.createInstance<TableContext>(_ctx, getState());
  enterRule(_localctx, 2, turingParser::RuleTable);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(36);
    match(turingParser::TABLE);
    setState(37);
    match(turingParser::COLON);
    setState(38);
    match(turingParser::ENDL);
    setState(40); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(39);
      stat();
      setState(42); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == turingParser::STATE);
    setState(47);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == turingParser::ENDL) {
      setState(44);
      match(turingParser::ENDL);
      setState(49);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StartContext ------------------------------------------------------------------

turingParser::StartContext::StartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::StartContext::ST() {
  return getToken(turingParser::ST, 0);
}

tree::TerminalNode* turingParser::StartContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

tree::TerminalNode* turingParser::StartContext::STATE() {
  return getToken(turingParser::STATE, 0);
}

std::vector<tree::TerminalNode *> turingParser::StartContext::ENDL() {
  return getTokens(turingParser::ENDL);
}

tree::TerminalNode* turingParser::StartContext::ENDL(size_t i) {
  return getToken(turingParser::ENDL, i);
}


size_t turingParser::StartContext::getRuleIndex() const {
  return turingParser::RuleStart;
}

void turingParser::StartContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStart(this);
}

void turingParser::StartContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStart(this);
}

turingParser::StartContext* turingParser::start() {
  StartContext *_localctx = _tracker.createInstance<StartContext>(_ctx, getState());
  enterRule(_localctx, 4, turingParser::RuleStart);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(50);
    match(turingParser::ST);
    setState(51);
    match(turingParser::COLON);
    setState(52);
    match(turingParser::STATE);
    setState(54); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(53);
      match(turingParser::ENDL);
      setState(56); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == turingParser::ENDL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlankContext ------------------------------------------------------------------

turingParser::BlankContext::BlankContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::BlankContext::BLANK() {
  return getToken(turingParser::BLANK, 0);
}

tree::TerminalNode* turingParser::BlankContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

turingParser::LiteralContext* turingParser::BlankContext::literal() {
  return getRuleContext<turingParser::LiteralContext>(0);
}

std::vector<tree::TerminalNode *> turingParser::BlankContext::ENDL() {
  return getTokens(turingParser::ENDL);
}

tree::TerminalNode* turingParser::BlankContext::ENDL(size_t i) {
  return getToken(turingParser::ENDL, i);
}


size_t turingParser::BlankContext::getRuleIndex() const {
  return turingParser::RuleBlank;
}

void turingParser::BlankContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBlank(this);
}

void turingParser::BlankContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBlank(this);
}

turingParser::BlankContext* turingParser::blank() {
  BlankContext *_localctx = _tracker.createInstance<BlankContext>(_ctx, getState());
  enterRule(_localctx, 6, turingParser::RuleBlank);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(58);
    match(turingParser::BLANK);
    setState(59);
    match(turingParser::COLON);
    setState(60);
    literal();
    setState(62); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(61);
      match(turingParser::ENDL);
      setState(64); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == turingParser::ENDL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InputContext ------------------------------------------------------------------

turingParser::InputContext::InputContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::InputContext::INP() {
  return getToken(turingParser::INP, 0);
}

tree::TerminalNode* turingParser::InputContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

tree::TerminalNode* turingParser::InputContext::STR() {
  return getToken(turingParser::STR, 0);
}

std::vector<tree::TerminalNode *> turingParser::InputContext::ENDL() {
  return getTokens(turingParser::ENDL);
}

tree::TerminalNode* turingParser::InputContext::ENDL(size_t i) {
  return getToken(turingParser::ENDL, i);
}


size_t turingParser::InputContext::getRuleIndex() const {
  return turingParser::RuleInput;
}

void turingParser::InputContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterInput(this);
}

void turingParser::InputContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitInput(this);
}

turingParser::InputContext* turingParser::input() {
  InputContext *_localctx = _tracker.createInstance<InputContext>(_ctx, getState());
  enterRule(_localctx, 8, turingParser::RuleInput);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(66);
    match(turingParser::INP);
    setState(67);
    match(turingParser::COLON);
    setState(68);
    match(turingParser::STR);
    setState(70); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(69);
      match(turingParser::ENDL);
      setState(72); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == turingParser::ENDL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatContext ------------------------------------------------------------------

turingParser::StatContext::StatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::StatContext::STATE() {
  return getToken(turingParser::STATE, 0);
}

tree::TerminalNode* turingParser::StatContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

std::vector<turingParser::ActionContext *> turingParser::StatContext::action() {
  return getRuleContexts<turingParser::ActionContext>();
}

turingParser::ActionContext* turingParser::StatContext::action(size_t i) {
  return getRuleContext<turingParser::ActionContext>(i);
}

std::vector<tree::TerminalNode *> turingParser::StatContext::ENDL() {
  return getTokens(turingParser::ENDL);
}

tree::TerminalNode* turingParser::StatContext::ENDL(size_t i) {
  return getToken(turingParser::ENDL, i);
}


size_t turingParser::StatContext::getRuleIndex() const {
  return turingParser::RuleStat;
}

void turingParser::StatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStat(this);
}

void turingParser::StatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStat(this);
}

turingParser::StatContext* turingParser::stat() {
  StatContext *_localctx = _tracker.createInstance<StatContext>(_ctx, getState());
  enterRule(_localctx, 10, turingParser::RuleStat);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    setState(102);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(74);
      match(turingParser::STATE);
      setState(75);
      match(turingParser::COLON);
      setState(76);
      action();
      setState(78); 
      _errHandler->sync(this);
      alt = 1;
      do {
        switch (alt) {
          case 1: {
                setState(77);
                match(turingParser::ENDL);
                break;
              }

        default:
          throw NoViableAltException(this);
        }
        setState(80); 
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx);
      } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(82);
      match(turingParser::STATE);
      setState(83);
      match(turingParser::COLON);
      setState(86); 
      _errHandler->sync(this);
      alt = 1;
      do {
        switch (alt) {
          case 1: {
                setState(84);
                match(turingParser::ENDL);
                setState(85);
                action();
                break;
              }

        default:
          throw NoViableAltException(this);
        }
        setState(88); 
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx);
      } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
      setState(91); 
      _errHandler->sync(this);
      alt = 1;
      do {
        switch (alt) {
          case 1: {
                setState(90);
                match(turingParser::ENDL);
                break;
              }

        default:
          throw NoViableAltException(this);
        }
        setState(93); 
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx);
      } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(95);
      match(turingParser::STATE);
      setState(96);
      match(turingParser::COLON);
      setState(98); 
      _errHandler->sync(this);
      alt = 1;
      do {
        switch (alt) {
          case 1: {
                setState(97);
                match(turingParser::ENDL);
                break;
              }

        default:
          throw NoViableAltException(this);
        }
        setState(100); 
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx);
      } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ActionContext ------------------------------------------------------------------

turingParser::ActionContext::ActionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

turingParser::SubContext* turingParser::ActionContext::sub() {
  return getRuleContext<turingParser::SubContext>(0);
}

tree::TerminalNode* turingParser::ActionContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

turingParser::PredContext* turingParser::ActionContext::pred() {
  return getRuleContext<turingParser::PredContext>(0);
}


size_t turingParser::ActionContext::getRuleIndex() const {
  return turingParser::RuleAction;
}

void turingParser::ActionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAction(this);
}

void turingParser::ActionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAction(this);
}

turingParser::ActionContext* turingParser::action() {
  ActionContext *_localctx = _tracker.createInstance<ActionContext>(_ctx, getState());
  enterRule(_localctx, 12, turingParser::RuleAction);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(104);
    sub();
    setState(105);
    match(turingParser::COLON);
    setState(106);
    pred();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PredContext ------------------------------------------------------------------

turingParser::PredContext::PredContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::PredContext::LC() {
  return getToken(turingParser::LC, 0);
}

tree::TerminalNode* turingParser::PredContext::RC() {
  return getToken(turingParser::RC, 0);
}

std::vector<turingParser::UnaryContext *> turingParser::PredContext::unary() {
  return getRuleContexts<turingParser::UnaryContext>();
}

turingParser::UnaryContext* turingParser::PredContext::unary(size_t i) {
  return getRuleContext<turingParser::UnaryContext>(i);
}

std::vector<turingParser::BinaryContext *> turingParser::PredContext::binary() {
  return getRuleContexts<turingParser::BinaryContext>();
}

turingParser::BinaryContext* turingParser::PredContext::binary(size_t i) {
  return getRuleContext<turingParser::BinaryContext>(i);
}

std::vector<tree::TerminalNode *> turingParser::PredContext::COMMA() {
  return getTokens(turingParser::COMMA);
}

tree::TerminalNode* turingParser::PredContext::COMMA(size_t i) {
  return getToken(turingParser::COMMA, i);
}


size_t turingParser::PredContext::getRuleIndex() const {
  return turingParser::RulePred;
}

void turingParser::PredContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPred(this);
}

void turingParser::PredContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPred(this);
}

turingParser::PredContext* turingParser::pred() {
  PredContext *_localctx = _tracker.createInstance<PredContext>(_ctx, getState());
  enterRule(_localctx, 14, turingParser::RulePred);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(126);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case turingParser::LC: {
        enterOuterAlt(_localctx, 1);
        setState(108);
        match(turingParser::LC);
        setState(111);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
        case 1: {
          setState(109);
          unary();
          break;
        }

        case 2: {
          setState(110);
          binary();
          break;
        }

        default:
          break;
        }
        setState(120);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == turingParser::COMMA) {
          setState(113);
          match(turingParser::COMMA);
          setState(116);
          _errHandler->sync(this);
          switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx)) {
          case 1: {
            setState(114);
            unary();
            break;
          }

          case 2: {
            setState(115);
            binary();
            break;
          }

          default:
            break;
          }
          setState(122);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(123);
        match(turingParser::RC);
        break;
      }

      case turingParser::MOVE: {
        enterOuterAlt(_localctx, 2);
        setState(125);
        unary();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnaryContext ------------------------------------------------------------------

turingParser::UnaryContext::UnaryContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::UnaryContext::MOVE() {
  return getToken(turingParser::MOVE, 0);
}


size_t turingParser::UnaryContext::getRuleIndex() const {
  return turingParser::RuleUnary;
}

void turingParser::UnaryContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUnary(this);
}

void turingParser::UnaryContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUnary(this);
}

turingParser::UnaryContext* turingParser::unary() {
  UnaryContext *_localctx = _tracker.createInstance<UnaryContext>(_ctx, getState());
  enterRule(_localctx, 16, turingParser::RuleUnary);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(128);
    match(turingParser::MOVE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BinaryContext ------------------------------------------------------------------

turingParser::BinaryContext::BinaryContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::BinaryContext::MOVE() {
  return getToken(turingParser::MOVE, 0);
}

tree::TerminalNode* turingParser::BinaryContext::COLON() {
  return getToken(turingParser::COLON, 0);
}

tree::TerminalNode* turingParser::BinaryContext::STATE() {
  return getToken(turingParser::STATE, 0);
}

tree::TerminalNode* turingParser::BinaryContext::WRITE() {
  return getToken(turingParser::WRITE, 0);
}

turingParser::LiteralContext* turingParser::BinaryContext::literal() {
  return getRuleContext<turingParser::LiteralContext>(0);
}


size_t turingParser::BinaryContext::getRuleIndex() const {
  return turingParser::RuleBinary;
}

void turingParser::BinaryContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBinary(this);
}

void turingParser::BinaryContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBinary(this);
}

turingParser::BinaryContext* turingParser::binary() {
  BinaryContext *_localctx = _tracker.createInstance<BinaryContext>(_ctx, getState());
  enterRule(_localctx, 18, turingParser::RuleBinary);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(136);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case turingParser::MOVE: {
        enterOuterAlt(_localctx, 1);
        setState(130);
        match(turingParser::MOVE);
        setState(131);
        match(turingParser::COLON);
        setState(132);
        match(turingParser::STATE);
        break;
      }

      case turingParser::WRITE: {
        enterOuterAlt(_localctx, 2);
        setState(133);
        match(turingParser::WRITE);
        setState(134);
        match(turingParser::COLON);
        setState(135);
        literal();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SubContext ------------------------------------------------------------------

turingParser::SubContext::SubContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

turingParser::LiteralContext* turingParser::SubContext::literal() {
  return getRuleContext<turingParser::LiteralContext>(0);
}

turingParser::ArrayContext* turingParser::SubContext::array() {
  return getRuleContext<turingParser::ArrayContext>(0);
}


size_t turingParser::SubContext::getRuleIndex() const {
  return turingParser::RuleSub;
}

void turingParser::SubContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSub(this);
}

void turingParser::SubContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSub(this);
}

turingParser::SubContext* turingParser::sub() {
  SubContext *_localctx = _tracker.createInstance<SubContext>(_ctx, getState());
  enterRule(_localctx, 20, turingParser::RuleSub);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(140);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case turingParser::CHAR:
      case turingParser::NUM: {
        enterOuterAlt(_localctx, 1);
        setState(138);
        literal();
        break;
      }

      case turingParser::LB: {
        enterOuterAlt(_localctx, 2);
        setState(139);
        array();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LiteralContext ------------------------------------------------------------------

turingParser::LiteralContext::LiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::LiteralContext::NUM() {
  return getToken(turingParser::NUM, 0);
}

tree::TerminalNode* turingParser::LiteralContext::CHAR() {
  return getToken(turingParser::CHAR, 0);
}


size_t turingParser::LiteralContext::getRuleIndex() const {
  return turingParser::RuleLiteral;
}

void turingParser::LiteralContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLiteral(this);
}

void turingParser::LiteralContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLiteral(this);
}

turingParser::LiteralContext* turingParser::literal() {
  LiteralContext *_localctx = _tracker.createInstance<LiteralContext>(_ctx, getState());
  enterRule(_localctx, 22, turingParser::RuleLiteral);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(142);
    _la = _input->LA(1);
    if (!(_la == turingParser::CHAR

    || _la == turingParser::NUM)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayContext ------------------------------------------------------------------

turingParser::ArrayContext::ArrayContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* turingParser::ArrayContext::LB() {
  return getToken(turingParser::LB, 0);
}

std::vector<turingParser::LiteralContext *> turingParser::ArrayContext::literal() {
  return getRuleContexts<turingParser::LiteralContext>();
}

turingParser::LiteralContext* turingParser::ArrayContext::literal(size_t i) {
  return getRuleContext<turingParser::LiteralContext>(i);
}

tree::TerminalNode* turingParser::ArrayContext::RB() {
  return getToken(turingParser::RB, 0);
}

std::vector<tree::TerminalNode *> turingParser::ArrayContext::COMMA() {
  return getTokens(turingParser::COMMA);
}

tree::TerminalNode* turingParser::ArrayContext::COMMA(size_t i) {
  return getToken(turingParser::COMMA, i);
}


size_t turingParser::ArrayContext::getRuleIndex() const {
  return turingParser::RuleArray;
}

void turingParser::ArrayContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArray(this);
}

void turingParser::ArrayContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<turingListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArray(this);
}

turingParser::ArrayContext* turingParser::array() {
  ArrayContext *_localctx = _tracker.createInstance<ArrayContext>(_ctx, getState());
  enterRule(_localctx, 24, turingParser::RuleArray);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(144);
    match(turingParser::LB);
    setState(145);
    literal();
    setState(150);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == turingParser::COMMA) {
      setState(146);
      match(turingParser::COMMA);
      setState(147);
      literal();
      setState(152);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(153);
    match(turingParser::RB);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

void turingParser::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  turingParserInitialize();
#else
  ::antlr4::internal::call_once(turingParserOnceFlag, turingParserInitialize);
#endif
}
