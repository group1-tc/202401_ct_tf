grammar turing;

// Lexer
WS     : [ \t\r\f]+ -> skip ;
COMMENT: '#'.*?'\n' -> skip;
TABLE  : 'table';
WRITE  : 'write';
BLANK  : 'blank';
INP    : 'input';
ST     : 'start state';
ENDL   : '\n';
MOVE   : [LR];
STATE  : [a-zA-Z_][a-zA-Z_0-9]* ;
CHAR   : '\'' . '\'' ;
NUM    : [0-9]+ ;
STR    : '\'' .*? '\'' ;
LB     : '[';
RB     : ']';
LC     : '{';
RC     : '}';
COMMA  : ',';
COLON  : ':';

// Parser
program : input? blank? start table EOF;

table: TABLE COLON ENDL stat+ ENDL*;
start: ST COLON STATE ENDL+;
blank: BLANK COLON literal ENDL+;
input: INP COLON STR ENDL+;

stat 
    : STATE COLON action ENDL+
    | STATE COLON (ENDL action)+ ENDL+
    | STATE COLON ENDL+
    ;

action : sub COLON pred;

pred 
    : LC (unary | binary) (COMMA (unary | binary))* RC
    | unary
    ;

unary : MOVE;

binary 
    : MOVE COLON STATE
    | WRITE COLON literal
    ;

sub : literal
    | array
    ;

literal 
    : NUM
    | CHAR
    ;

array : LB literal (COMMA literal)* RB;
