
// Generated from turing.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "turingParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by turingParser.
 */
class  turingListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterProgram(turingParser::ProgramContext *ctx) = 0;
  virtual void exitProgram(turingParser::ProgramContext *ctx) = 0;

  virtual void enterTable(turingParser::TableContext *ctx) = 0;
  virtual void exitTable(turingParser::TableContext *ctx) = 0;

  virtual void enterStart(turingParser::StartContext *ctx) = 0;
  virtual void exitStart(turingParser::StartContext *ctx) = 0;

  virtual void enterBlank(turingParser::BlankContext *ctx) = 0;
  virtual void exitBlank(turingParser::BlankContext *ctx) = 0;

  virtual void enterInput(turingParser::InputContext *ctx) = 0;
  virtual void exitInput(turingParser::InputContext *ctx) = 0;

  virtual void enterStat(turingParser::StatContext *ctx) = 0;
  virtual void exitStat(turingParser::StatContext *ctx) = 0;

  virtual void enterAction(turingParser::ActionContext *ctx) = 0;
  virtual void exitAction(turingParser::ActionContext *ctx) = 0;

  virtual void enterPred(turingParser::PredContext *ctx) = 0;
  virtual void exitPred(turingParser::PredContext *ctx) = 0;

  virtual void enterUnary(turingParser::UnaryContext *ctx) = 0;
  virtual void exitUnary(turingParser::UnaryContext *ctx) = 0;

  virtual void enterBinary(turingParser::BinaryContext *ctx) = 0;
  virtual void exitBinary(turingParser::BinaryContext *ctx) = 0;

  virtual void enterSub(turingParser::SubContext *ctx) = 0;
  virtual void exitSub(turingParser::SubContext *ctx) = 0;

  virtual void enterLiteral(turingParser::LiteralContext *ctx) = 0;
  virtual void exitLiteral(turingParser::LiteralContext *ctx) = 0;

  virtual void enterArray(turingParser::ArrayContext *ctx) = 0;
  virtual void exitArray(turingParser::ArrayContext *ctx) = 0;


};

