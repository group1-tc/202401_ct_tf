#pragma once

#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>
#include <algorithm>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>
#include <map>

class AST {
public:
    virtual ~AST() = default;
    virtual void print() const = 0;
    virtual llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)= 0; 
};

class ASTchar : public AST {
private:
    char c;
public:
    ASTchar(char c) : c(c) {}
    void print() const override;
    char getChar() const { return c; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTaction : public AST {
private:
    char action;
    std::string predicate;
public:
    ASTaction(char action, std::string predicate) : action(action), predicate(std::move(predicate)) {}
    void print() const override;
    char getAction() const { return action; }
    std::string getPredicate() const { return predicate; }
    void setPredicate(std::string pred) { if(predicate == "") predicate = std::move(pred); }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTpredicate : public AST {
private:
    std::vector<std::unique_ptr<ASTaction>> actions;
public:
    ASTpredicate(std::vector<std::unique_ptr<ASTaction>> actions) : actions(std::move(actions)) {}
    void print() const override;
    std::vector<std::unique_ptr<ASTaction>>& getActions()  { return actions; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTinstruction : public AST {
private:
    std::vector<char> condition;
    std::vector<std::unique_ptr<ASTaction>> actions;
public:
    ASTinstruction(std::vector<char> condition, std::vector<std::unique_ptr<ASTaction>> actions) : condition(std::move(condition)), actions(std::move(actions)) {}
    void print() const override;
    std::vector<char> getCondition() const { return condition; }
    const std::vector<std::unique_ptr<ASTaction>>& getActions() const { return actions; }
    void fill(std::string name);
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTname : public AST {
private:
    std::string name;
public:
    ASTname(std::string name) : name(std::move(name)) {}
    void print() const override;
    std::string getName() const { return name; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTstate : public AST {
private:
    std::string name;
    std::vector<std::unique_ptr<ASTinstruction>> instructions;
public:
    ASTstate(std::string name, std::vector<std::unique_ptr<ASTinstruction>> instructions) : name(std::move(name)), instructions(std::move(instructions)) {}
    void print() const override;
    std::string getName() const { return name; }
    const std::vector<std::unique_ptr<ASTinstruction>>& getInstructions() const { return instructions; }
    void fill();
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTtable : public AST {
private:
    std::vector<std::unique_ptr<ASTstate>> states;
public:
    ASTtable(std::vector<std::unique_ptr<ASTstate>> states) : states(std::move(states)) {}
    void print() const override;
    std::vector<std::unique_ptr<ASTstate>>& getStates() { return states; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTblank : public AST {
private:
    char c;
public:
    ASTblank(char c) : c(c) {}
    void print() const override;
    char getChar() const { return c; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTtape : public AST {
private:
    std::string tape;
public:
    ASTtape(std::string tape) : tape(std::move(tape)) {}
    void print() const override;
    std::string getTape() const { return tape; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTstart : public AST {
private:
    std::string start;
public:
    ASTstart(std::string start) : start(std::move(start)) {}
    void print() const override;
    std::string getStart() const { return start; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTprogram : public AST {
private:
    char blank;
    std::string start;
    std::string tape;
    std::vector<std::unique_ptr<ASTstate>> table;
public:
    ASTprogram(std::vector<std::unique_ptr<ASTstate>> table, char blank, std::string start, std::string tape) : table(std::move(table)), blank(blank), start(std::move(start)), tape(std::move(tape)) {}
    void print() const override;
    char getBlank() const { return blank; }
    std::string getStart() const { return start; }
    std::string getTape() const { return tape; }
    std::vector<std::unique_ptr<ASTstate>>& getTable() { return table; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};

class ASTarray : public AST {
private:
    std::vector<char> array;
public:
    ASTarray(std::vector<char> array) : array(std::move(array)) {}
    void print() const override;
    const std::vector<char>& getArray() const { return array; }
    llvm::Value* codegen(llvm::LLVMContext &context, llvm::IRBuilder<> &builder, llvm::Module &module, std::map<std::string, llvm::BasicBlock*> &blocks, llvm::Value* arrayPtr, llvm::Value* &pointer)override;
};
