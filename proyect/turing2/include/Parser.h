#pragma once

#include <string>
#include <memory>
#include "AST.h"

std::unique_ptr<ASTprogram> generateAST(const std::string& program);
std::string readFile(const std::string& filename);
