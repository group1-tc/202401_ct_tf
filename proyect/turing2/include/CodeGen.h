#pragma once

#include "AST.h"
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>

void generateLLVMIR(std::unique_ptr<ASTprogram> &ast, const std::string &filename);
