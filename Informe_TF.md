---
title: Introduccion

---

# Informe del Trabajo Final de Compiladores

## Introduccion
En el ámbito de la ciencia de la computación, las máquinas de Turing representan un pilar fundamental en la teoría de la computabilidad y la computación teórica. Estas máquinas, concebidas por Alan Turing en 1936, han servido como modelo abstracto para entender los límites y capacidades de los algoritmos y los sistemas computacionales. El presente proyecto se centra en la implementación de una aplicación innovadora que no solo compila y ejecuta máquinas de Turing, sino que además ofrece funcionalidades avanzadas como la generación de representaciones visuales en forma de grafo y la ejecución Just-In-Time (JIT).

#### Problema y Motivación
La complejidad inherente a la comprensión y ejecución de máquinas de Turing puede representar un desafío significativo para estudiantes y profesionales de la informática. La falta de herramientas accesibles y robustas que faciliten la definición, compilación y ejecución de estas máquinas puede limitar el aprendizaje y la investigación en este campo crucial de la teoría de la computación. Por lo tanto, surge la necesidad de desarrollar una solución que no solo simplifique estos procesos, sino que también los enriquezca con capacidades adicionales, como la visualización gráfica y la ejecución dinámica.

#### Solución Propuesta
Para abordar estas problemáticas, nuestro proyecto propone una plataforma integral que permita a los usuarios definir máquinas de Turing mediante un lenguaje específico y versátil. Este lenguaje no solo contemplará la estructura básica de una máquina de Turing, incluyendo el input, el alfabeto y la configuración de la cinta, sino que también facilitará la generación de código ejecutable y la representación gráfica en formato DOT. Además, se incorporará la capacidad de ejecución JIT para optimizar el rendimiento durante la ejecución de las máquinas definidas.


## Objetivos
- Definir un lenguaje de representación para máquinas de Turing que sea completo y flexible, permitiendo configurar la máquina, incluyendo el alfabeto, los estados, la cinta y las transiciones.
- Implementar un compilador que traduzca la definición de la máquina de Turing a código intermedio (IR) utilizando LLVM, y que posteriormente genere un ejecutable.
- Desarrollar un ejecutor JIT (Just-In-Time) que permita la ejecución dinámica y eficiente de la máquina de Turing.
- Generar representaciones visuales de la máquina de Turing en formato DOT, compatible con Graphviz, para facilitar la visualización y análisis del comportamiento de la máquina.

## Marco Teorico 
#### Máquinas de Turing
Las máquinas de Turing, ideadas por Alan Turing en 1936, son dispositivos abstractos fundamentales en la teoría de la computación. Están compuestas por una cinta infinita dividida en casillas, una cabeza lectora/escritora que opera sobre la cinta, y un conjunto finito de estados de control que guían las operaciones de la máquina. Este modelo teórico es capaz de simular cualquier algoritmo computable, estableciéndose como el estándar para entender la computabilidad y la complejidad computacional.

#### Teoría de la Computabilidad
La teoría de máquinas de Turing es esencial para la comprensión de la computabilidad, que se refiere a la capacidad de un problema para ser resuelto de manera algorítmica. Este campo de estudio explora qué problemas pueden ser computados y cuáles no, estableciendo límites teóricos fundamentales en la resolución de problemas.

#### Lenguajes Formales y Autómatas
Relacionados estrechamente con las máquinas de Turing, los lenguajes formales y los autómatas son herramientas para describir y analizar la estructura y comportamiento de los sistemas computacionales. Los autómatas, como las máquinas de Turing deterministas y no deterministas, son modelos matemáticos que desempeñan un papel crucial en la clasificación de problemas según su complejidad computacional.

#### Aplicaciones y Relevancia Actual
Aunque concebidas hace décadas, las máquinas de Turing siguen siendo relevantes en la era moderna de la computación. Han sentado las bases teóricas para el diseño de lenguajes de programación, la optimización de algoritmos y la teoría de compiladores. Además, inspiran desarrollos en inteligencia artificial, sistemas distribuidos y la teoría de la computación cuántica.

#### Implementación Práctica
La implementación práctica de máquinas de Turing ha evolucionado con el tiempo, gracias al desarrollo de herramientas y entornos de simulación avanzados. Estas permiten la experimentación y verificación de algoritmos en contextos controlados, facilitando así la investigación en verificación formal de software y optimización de rendimiento computacional.


## Metodologia
1. Generar el Lexer y Parser con ANTLR
2. Generación de IR con LLVM
3. Ejecución JIT con LLVM
4. Generación de Representaciones DOT
5. Generacion de Pruebas



## Conclusiones
- La creación de una herramienta para compilar y ejecutar máquinas de Turing no solo tiene aplicaciones prácticas en el desarrollo de software y la optimización de algoritmos, sino que también ofrece un recurso educativo invaluable. Facilita el aprendizaje y la experimentación en un campo teórico denso pero fundamental, permitiendo a estudiantes y profesionales explorar conceptos avanzados de forma accesible y práctica. Este proyecto abre nuevas puertas para la investigación en teoría de compiladores y sistemas formales, contribuyendo al avance continuo de la ciencia computacional.

- A pesar de haber sido concebidas hace casi un siglo, las máquinas de Turing siguen siendo relevantes en la era digital actual. Su influencia se extiende a campos tan diversos como la inteligencia artificial, los sistemas distribuidos y la teoría de la computación cuántica. Estos dispositivos teóricos no solo son una herramienta educativa invaluable, sino que también inspiran y fundamentan avances tecnológicos significativos que moldean el futuro de la computación y la informática.
- La implementación de un ejecutor JIT (Just-In-Time) para las máquinas de Turing representa una contribución significativa a la eficiencia y flexibilidad en la ejecución de estos modelos. Este enfoque no solo optimiza el rendimiento al traducir y ejecutar código en tiempo real, sino que también permite adaptar dinámicamente las simulaciones a diferentes contextos y necesidades, facilitando la experimentación y el análisis en tiempo real.
- La generación de representaciones visuales en formato DOT, compatibles con Graphviz, añade una dimensión visual al estudio de las máquinas de Turing, haciendo más accesible la comprensión de su comportamiento y estructuras. Esta herramienta no solo beneficia a los investigadores y desarrolladores en la visualización y debugging de las máquinas de Turing, sino que también es una valiosa herramienta pedagógica que puede ayudar a estudiantes a visualizar y entender mejor los conceptos abstractos de la computación teórica.
